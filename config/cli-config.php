<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

require __DIR__.'/../vendor/autoload.php';

$config = require 'config.php';
$config = $config['doctrine'];

$configuration = Setup::createAnnotationMetadataConfiguration(
    $config['meta']['entity_path'],
    $config['meta']['auto_generate_proxies'],
    $config['meta']['proxy_dir'],
    $config['meta']['cache'],
    false
);

$em = EntityManager::create($config['connection'], $configuration);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($em);