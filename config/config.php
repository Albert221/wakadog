<?php

return [
    'debug' => true,
    'locale' => '',
    'doctrine' => [
        'meta' => [
            'entity_path' => [
                __DIR__.'/../src/Entity'
            ],
            'auto_generate_proxies' => true,
            'proxy_dir' => __DIR__.'/../cache/proxies',
            'cache' => null
        ],
        'connection' => [
            'driver' => 'pdo_mysql',
            'host' => 'localhost',
            'dbname' => 'wakadog',
            'user' => 'root',
            'password' => '',
            'charset' => 'utf8'
        ],
        'dql_extensions' => [
            'numeric' => [
                'acos' => 'DoctrineExtensions\Query\Mysql\Acos',
                'cos' => 'DoctrineExtensions\Query\Mysql\Cos',
                'sin' => 'DoctrineExtensions\Query\Mysql\Sin'
            ]
        ]
    ],
    'twig' => [
//        'cache' => __DIR__.'/../cache/views',
        'debug' => true,
        'globals' => [
            'gmaps_api_key' => 'AIzaSyAJP9tl9517VC2lv-6OyqYFxwtBoY2RtJk'
        ]
    ]
];