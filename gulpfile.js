const gulp 			= require('gulp'),
    sass 			= require('gulp-sass'),
    autoprefixer	= require('gulp-autoprefixer'),
    csso			= require('gulp-csso');

gulp.task('sass', () => {
    return gulp.src('resources/sass/style.scss')
               .pipe(sass().on('error', sass.logError))
               .pipe(autoprefixer())
               .pipe(csso())
               .pipe(gulp.dest('web/assets'));
});

gulp.task('sass:watch', () => {
    gulp.watch('resources/sass/*.scss', ['sass']);
});

gulp.task('images', () => {
    return gulp.src('resources/images/*')
               .pipe(gulp.dest('web/assets/images'));
});

gulp.task('fonts', () => {
    return gulp.src('node_modules/materialize-css/fonts/roboto/*')
               .pipe(gulp.dest('web/assets/fonts'));
});

gulp.task('scripts', () => {
    return gulp.src(['node_modules/jquery/dist/jquery.min.js',
        'node_modules/materialize-css/dist/js/materialize.min.js'])
               .pipe(gulp.dest('web/assets/'));
});

gulp.task('default', ['sass', 'images', 'fonts', 'scripts']);