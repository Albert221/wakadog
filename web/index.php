<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Slim\App;
use Wakadog\Controller\RatingController;
use Wakadog\Flash\Messages as Flash;
use Slim\Views\Twig;
use Slim\Views\TwigExtension as ViewsExtension;
use Wakadog\Controller\ControllerFactory;
use Wakadog\Controller\DogController;
use Wakadog\Controller\LandingController;
use Wakadog\Controller\UserController;
use Wakadog\Entity\Dog;
use Wakadog\Entity\Rating;
use Wakadog\Entity\User;
use Wakadog\Repository\DogRepository;
use Wakadog\Repository\RatingRepository;
use Wakadog\Repository\UserRepository;
use Wakadog\Security\Authenticator;
use Wakadog\Security\Middleware\FirewallMiddleware;
use Wakadog\Security\Session\PhpUserSessionManager;
use Wakadog\Twig\SecurityExtension;
use Wakadog\Twig\ToastExtension;

require '../vendor/autoload.php';

session_start();

$app = new App([
    'settings' => [
        'determineRouteBeforeAppMiddleware' => true
    ]
]);

$c = $app->getContainer();
$c['config'] = require '../config/config.php';
$c['foundHandler'] = function () {
    return new \Slim\Handlers\Strategies\RequestResponseArgs();
};

$c['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        if ($exception instanceof \Wakadog\ForbiddenException) {
            return $response->withHeader('Location', $c['router']->pathFor('user.signIn'));
        }

        return $response;
    };
};

if ($c['config']['debug']) {
    $c['settings']['displayErrorDetails'] = true;
}

ini_set('intl.default_locale', $c['config']['locale']);

/************************************************
 * Container setup
 ************************************************/

$c['view'] = function ($c) {
    $twig = new Twig(__DIR__.'/../resources/views', $c['config']['twig']);

    $basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');
    $twig->addExtension(new ViewsExtension($c['router'], $basePath));
    $twig->addExtension(new SecurityExtension($c['userSessionManager']));
    $twig->addExtension(new ToastExtension($c['flash']));
    $twig->addExtension(new Twig_Extensions_Extension_Intl());

    if ($c['config']['debug']) {
        $twig->addExtension(new Twig_Extension_Debug());
    }

    foreach ($c['config']['twig']['globals'] as $key => $value) {
        $twig->getEnvironment()->addGlobal($key, $value);
    }

    return $twig;
};

$c['em'] = function ($c) {
    $config = $c['config']['doctrine'];

    $configuration = Setup::createAnnotationMetadataConfiguration(
        $config['meta']['entity_path'],
        $config['meta']['auto_generate_proxies'],
        $config['meta']['proxy_dir'],
        $config['meta']['cache'],
        false
    );

    foreach($config['dql_extensions']['numeric'] as $name => $class) {
        $configuration->addCustomNumericFunction($name, $class);
    }

    return EntityManager::create($config['connection'], $configuration);
};

$c['userSessionManager'] = function ($c) {
    return new PhpUserSessionManager($c[UserRepository::class]);
};

$c['authenticator'] = function ($c) {
    return new Authenticator($c[UserRepository::class], $c['userSessionManager']);
};

$c['flash'] = function () {
    return new Flash();
};

$c['controllerFactory'] = function ($c) {
    return new ControllerFactory($c['view'], $c['flash'], $c['userSessionManager'], $c['router']);
};

$c[DogRepository::class] = function ($c) {
    return $c['em']->getRepository(Dog::class);
};

$c[RatingRepository::class] = function ($c) {
    return $c['em']->getRepository(Rating::class);
};

$c[UserRepository::class] = function ($c) {
    return $c['em']->getRepository(User::class);
};

$c[DogController::class] = function ($c) {
    return $c['controllerFactory']->create(DogController::class, [
        $c[DogRepository::class],
        $c[RatingRepository::class]
    ]);
};

$c[UserController::class] = function ($c) {
    return $c['controllerFactory']->create(UserController::class, [
        $c['authenticator'],
        $c[UserRepository::class]
    ]);
};

$c[RatingController::class] = function ($c) {
    return $c['controllerFactory']->create(RatingController::class, [
        $c[RatingRepository::class],
        $c[DogRepository::class]
    ]);
};

$c[LandingController::class] = function ($c) {
    return $c['controllerFactory']->create(LandingController::class);
};

/************************************************
 * Middlewares
 ************************************************/

$app->add(new FirewallMiddleware($c['userSessionManager']));

/************************************************
 * Routes
 ************************************************/

$app->get('/', 'Wakadog\Controller\LandingController')->setName('landing');

$app->get('/map', 'Wakadog\Controller\DogController:map')->setName('dog.map');
$app->get('/dashboard', 'Wakadog\Controller\DogController:dashboard')->setName('dog.dashboard');
$app->get('/dog/{id:[0-9]+}', 'Wakadog\Controller\DogController:dog')->setName('dog.dog');
$app->get('/dog/add', 'Wakadog\Controller\DogController:add')->setName('dog.add');
$app->post('/dog', 'Wakadog\Controller\DogController:store')->setName('dog.store');
$app->get('/dog/{id:[0-9]+}/edit', 'Wakadog\Controller\DogController:edit')->setName('dog.edit');
$app->patch('/dog/{id:[0-9]+}', 'Wakadog\Controller\DogController:update')->setName('dog.update');
$app->delete('/dog/{id:[0-9]+}', 'Wakadog\Controller\DogController:remove')->setName('dog.remove');

$app->post('/rating', 'Wakadog\Controller\RatingController:add')->setName('rating.add');
$app->delete('/rating/{id:[0-9]+}', 'Wakadog\Controller\RatingController:remove')->setName('rating.remove');

$app->get('/signin', 'Wakadog\Controller\UserController:signIn')->setName('user.signIn');
$app->post('/signin', 'Wakadog\Controller\UserController:authenticate')->setName('user.authenticate');
$app->get('/signup', 'Wakadog\Controller\UserController:signUp')->setName('user.signUp');
$app->post('/signup', 'Wakadog\Controller\UserController:register')->setName('user.register');
$app->get('/signout', 'Wakadog\Controller\UserController:signOut')->setName('user.signOut');
$app->get('/settings', 'Wakadog\Controller\UserController:settings')->setName('user.settings');
$app->post('/settings', 'Wakadog\Controller\UserController:updateSettings')->setName('user.updateSettings');
$app->post('/settings/change-password', 'Wakadog\Controller\UserController:changePassword')->setName('user.changePassword');
$app->delete('/settings/delete-account', 'Wakadog\Controller\UserController:deleteAccount')->setName('user.deleteAccount');
$app->post('/user/{id:[0-9]+}/ban', 'Wakadog\Controller\UserController:ban')->setName('user.ban');

$app->run();