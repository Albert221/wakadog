# Wakadog

Wakadog is a web application for dog owners which don't have time to walk them. Wakadog can help those people in finding people who can help them with it.

## Requirements
- PHP 5.6 with
    - Intl extension (for localization of dates)
    - GD extension (for operating on uploaded images)
    - Exif extension (for operating on uploaded images)
- npm with gulp globally installed
- composer
- MySQL database

## Installation

- Through git (**RECOMMENDED**)
    1. Clone repository with `git clone https://gitlab.com/Albert221/wakadog.git`.
    2. Install npm dependencies with `npm install`.
    3. Build scripts and stylesheets with `gulp`.
    4. Install composer dependencies with `composer install`.
- With preinstalled and prebuilded package (**NOT RECOMMENDED**)
    1. Download zip with all files from the newest tag.
    
### And then...
1. Change database connection configuration and GMaps API key in `config/config.php` file.
2. Give `vendor/doctrine/orm/bin/doctrine` permission for executing, e.g. `chmod +x vendor/doctrine/orm/bin/doctrine`.
3. Run `vendor/doctrine/orm/bin/doctrine orm:schema-tool:create`.
4. Give `web/storage` permisison for writing.

And you're done! You can register and *do stuff*.

You can also give yourself moderator privileges by setting `role` to `mod` in database.