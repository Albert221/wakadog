<?php

namespace Wakadog\Security;

use Wakadog\Security\Role\Role;

interface User
{
    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getPassword();

    /**
     * @return Role
     */
    public function getRole();
}