<?php

namespace Wakadog\Security;

use Wakadog\Security\Role\Role;
use Wakadog\Security\Role\RoleManager;

class GuestUser implements User
{
    private $role;

    public function __construct()
    {
        $this->role = RoleManager::create('guest');
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return '';
    }

    /**
     * @return Role
     */
    public function getRole()
    {
        return $this->role;
    }
}