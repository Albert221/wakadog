<?php

namespace Wakadog\Security;

use Wakadog\Repository\UserRepository;
use Wakadog\Security\Session\UserSessionManager;

class Authenticator
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserSessionManager
     */
    private $userSessionManager;

    /**
     * Authenticator constructor.
     *
     * @param UserRepository $userRepository
     * @param UserSessionManager $userSessionManager
     */
    public function __construct(UserRepository $userRepository, UserSessionManager $userSessionManager)
    {
        $this->userRepository = $userRepository;
        $this->userSessionManager = $userSessionManager;
    }

    /**
     * Gets user from database, checks password and loggs in
     *
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function authenticate($email, $password)
    {
        $user = $this->userRepository->byEmail($email);

        if ($user && password_verify($password, $user->getPassword()) && ! $user->isBanned()) {

            $this->userSessionManager->signIn($user);

            return true;
        }

        return false;
    }

    /**
     * Logs te user out
     *
     * @return void
     */
    public function logout()
    {
        $this->userSessionManager->signOut();
    }
}