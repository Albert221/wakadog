<?php

namespace Wakadog\Security\Session;

use RuntimeException;
use Wakadog\Repository\UserRepository;
use Wakadog\Security\GuestUser;
use Wakadog\Security\User;

class PhpUserSessionManager implements UserSessionManager
{
    /**
     * @var User|null Cached user
     */
    private $currentUser = null;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * PhpUserSessionManager constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if (is_null($this->currentUser)) {
            $this->instantiateUser();
        }

        return $this->currentUser;
    }

    /**
     * Unserializes user from session if logged in, otherwise creates guest user
     */
    private function instantiateUser()
    {
        if (isset($_SESSION['user'])) {
            // User exists in session so he's logged in
            $user = unserialize($_SESSION['user']);

            if (! $user) {
                throw new RuntimeException('User object from session could not be unserialized');
            }

            // If user won't be managed by EntityManager then all operations
            // featuring this object will cause persistency problems
            // TODO: Optimize this so it won't query DB on each request
            $user = $this->userRepository->find($user->getId());
        } else {
            // User doesn't exist in session so he's a guest
            $user = new GuestUser();
        }

        $this->currentUser = $user;
    }

    public function isSignedIn()
    {
        return isset($_SESSION['user']);
    }

    /**
     * @param User $user
     */
    public function signIn(User $user)
    {
        if ($this->isSignedIn()) {
            throw new RuntimeException('User already signed in');
        }

        $this->currentUser = $_SESSION['user'] = serialize($user);
    }

    public function signOut()
    {
        if (! $this->isSignedIn()) {
            throw new RuntimeException('User already signed out');
        }

        unset($this->currentUser, $_SESSION['user']);
    }
}