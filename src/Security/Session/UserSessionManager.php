<?php

namespace Wakadog\Security\Session;

use Wakadog\Security\User;

interface UserSessionManager
{
    /**
     * @return User
     */
    public function getUser();

    /**
     * @return bool
     */
    public function isSignedIn();

    /**
     * @param User $user
     */
    public function signIn(User $user);

    /**
     * @return void
     */
    public function signOut();
}