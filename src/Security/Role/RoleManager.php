<?php

namespace Wakadog\Security\Role;

use RuntimeException;
use Symfony\Component\Yaml\Yaml;

class RoleManager
{
    /**
     * @var Role[]
     */
    private static $roles;

    /**
     * Returns specified role
     *
     * @param string $roleName
     * @return Role
     */
    public static function create($roleName)
    {
        if (is_null(self::$roles)) {
            self::loadRoles();
        }

        if (! isset(self::$roles[$roleName])) {
            throw new RuntimeException(sprintf('There is no %s role', $roleName));
        }

        return clone self::$roles[$roleName];
    }

    /**
     * Loads roles from YAML file at /config/roles.yml
     */
    private static function loadRoles()
    {
        $rolesFile = dirname(dirname(dirname(__DIR__))).'/config/roles.yml';
        $yamlArray = Yaml::parse(file_get_contents($rolesFile), Yaml::PARSE_EXCEPTION_ON_INVALID_TYPE);

        foreach ($yamlArray['roles'] as $roleName => $roleConfig) {
            $role = new Role($roleName, $roleConfig['privileges']);

            if (isset($roleConfig['parent'])) {
                $parentRole = self::create($roleConfig['parent']);
                $parentPrivileges = $parentRole->getPrivileges();

                foreach ($parentPrivileges as $parentPrivilege) {
                    // Iterate through parent's privileges and add them to child
                    if ($role->can(sprintf('-%s', $parentPrivilege))) {
                        // But give ability to not inherit privilege from parent
                        continue;
                    }

                    $role->addPrivilenge($parentPrivilege);
                }
            }

            self::$roles[$roleName] = $role;
        }
    }
}