<?php

namespace Wakadog\Security\Role;

class Role
{
    private $name;
    private $privileges = [];

    /**
     * Role constructor.
     *
     * @param string $name
     * @param array $privileges
     */
    public function __construct($name, array $privileges)
    {
        $this->name = $name;
        $this->privileges = $privileges;
    }

    /**
     * @param string $privilege
     * @return bool
     */
    public function can($privilege)
    {
        return in_array($privilege, $this->privileges);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getPrivileges()
    {
        return $this->privileges;
    }

    /**
     * @param string $privilenge
     */
    public function addPrivilenge($privilenge)
    {
        $this->privileges[] = $privilenge;
    }
}