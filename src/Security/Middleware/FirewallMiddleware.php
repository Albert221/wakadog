<?php

namespace Wakadog\Security\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Route;
use Wakadog\ForbiddenException;
use Wakadog\Security\Session\UserSessionManager;

class FirewallMiddleware
{
    private $userSessionManager;

    /**
     * FirewallMiddleware constructor.
     *
     * @param UserSessionManager $userSessionManager
     */
    public function __construct(UserSessionManager $userSessionManager)
    {
        $this->userSessionManager = $userSessionManager;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     * @throws ForbiddenException
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        /** @var Route $route */
        $route = $request->getAttribute('route');
        $requiredPrivilege = sprintf('route.%s', $route->getName());

        $currentUser = $this->userSessionManager->getUser();

        if (! $currentUser->getRole()->can($requiredPrivilege)) {
            throw new ForbiddenException();
        }

        return $next($request, $response);
    }
}