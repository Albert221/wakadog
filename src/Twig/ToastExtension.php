<?php

namespace Wakadog\Twig;

use Slim\Flash\Messages as Flash;
use Twig_Extension;
use Twig_Extension_GlobalsInterface;

class ToastExtension extends Twig_Extension implements Twig_Extension_GlobalsInterface
{
    /**
     * @var Flash
     */
    private $flash;

    /**
     * ToastExtension constructor.
     *
     * @param Flash $flash
     */
    public function __construct(Flash $flash)
    {
        $this->flash = $flash;
    }

    /**
     * {@inheritdoc}
     */
    public function getGlobals()
    {
        return [
            'toasts' => $this->flash->getMessage('toast')
        ];
    }
}