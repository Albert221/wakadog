<?php

namespace Wakadog\Twig;

use Twig_Extension;
use Twig_Extension_GlobalsInterface;
use Twig_SimpleFunction;
use Wakadog\Security\Session\UserSessionManager;

class SecurityExtension extends Twig_Extension implements Twig_Extension_GlobalsInterface
{
    /**
     * @var UserSessionManager
     */
    private $userSessionManager;

    /**
     * SecurityExtension constructor.
     *
     * @param UserSessionManager $userSessionManager
     */
    public function __construct(UserSessionManager $userSessionManager)
    {
        $this->userSessionManager = $userSessionManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('can', [$this, 'can']),
            new Twig_SimpleFunction('isLoggedIn', [$this, 'isLoggedIn']),
        ];
    }

    /**
     * @param string $privilege
     * @return bool
     */
    public function can($privilege)
    {
        $user = $this->userSessionManager->getUser();

        return $user->getRole()->can($privilege);
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->userSessionManager->isSignedIn();
    }

    /**
     * {@inheritdoc}
     */
    public function getGlobals()
    {
        return [
            'user' => $this->userSessionManager->getUser()
        ];
    }
}