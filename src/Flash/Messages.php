<?php

namespace Wakadog\Flash;

use Slim\Flash\Messages as ParentMessages;

class Messages extends ParentMessages
{
    /**
     * Unlikely the addMessage method, you can set array or anything else with this
     *
     * @param string $key
     * @param mixed$message
     */
    public function setMessage($key, $message)
    {
        $this->storage[$this->storageKey][$key] = $message;
    }
}