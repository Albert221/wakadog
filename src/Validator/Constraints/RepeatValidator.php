<?php

namespace Wakadog\Validator\Constraints;

use Symfony\Component\Validator\Constraints\AbstractComparisonValidator;

class RepeatValidator extends AbstractComparisonValidator
{
    protected function compareValues($value1, $value2)
    {
        return $value1 == $value2;
    }
}