<?php

namespace Wakadog\Validator\Constraints;

use Symfony\Component\Validator\Constraints\AbstractComparison;

class Repeat extends AbstractComparison
{
    public $message = 'This value should be the same as previous.';
}