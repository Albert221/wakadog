<?php

namespace Wakadog\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Wakadog\Repository\Database\RatingRepository")
 * @ORM\Table(name="ratings")
 */
class Rating
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $rating;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ratings")
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity="Dog", inversedBy="ratings")
     */
    private $dog;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    /**
     * @return Dog
     */
    public function getDog()
    {
        return $this->dog;
    }

    /**
     * @param Dog $dog
     */
    public function setDog(Dog $dog)
    {
        $this->dog = $dog;
    }
}
