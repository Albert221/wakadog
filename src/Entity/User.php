<?php

namespace Wakadog\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Wakadog\Security\Role\Role;
use Wakadog\Security\Role\RoleManager;
use Wakadog\Security\User as SecurityUser;

/**
 * @ORM\Entity(repositoryClass="Wakadog\Repository\Database\UserRepository")
 * @ORM\Table(name="users")
 */
class User implements SecurityUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="visible_name")
     */
    private $visibleName;

    /**
     * @ORM\Column
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $banned = false;

    /**
     * @ORM\Column(name="role")
     */
    private $roleName = 'user';

    /**
     * @ORM\Column(type="datetime", name="created_date")
     */
    private $createdDate;

    /**
     * @ORM\Column(type="datetime", name="last_login_date")
     */
    private $lastLoginDate;

    /**
     * @ORM\OneToMany(targetEntity="Dog", mappedBy="owner", cascade={"persist", "remove"})
     */
    private $dogs;

    /**
     * @ORM\OneToMany(targetEntity="Rating", mappedBy="author", cascade={"persist", "remove"})
     */
    private $ratings;

    /**
     * @var Role|null
     */
    private $role = null;

    public function __construct()
    {
        $this->dogs = new ArrayCollection();
        $this->ratings = new ArrayCollection();

        $this->createdDate = new DateTime();
        $this->lastLoginDate = new DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getVisibleName()
    {
        return $this->visibleName;
    }

    /**
     * @param string $visibleName
     */
    public function setVisibleName($visibleName)
    {
        $this->visibleName = $visibleName;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @return bool
     */
    public function isBanned()
    {
        return $this->banned;
    }

    /**
     * @param bool $banned
     */
    public function setBanned($banned)
    {
        $this->banned = $banned;
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
        return $this->roleName;
    }

    /**
     * @param string $roleName
     */
    public function setRoleName($roleName)
    {
        $this->roleName = $roleName;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param DateTime $createdDate
     */
    public function setCreatedDate(DateTime $createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return DateTime
     */
    public function getLastLoginDate()
    {
        return $this->lastLoginDate;
    }

    /**
     * @param DateTime $lastLoginDate
     */
    public function setLastLoginDate(DateTime $lastLoginDate)
    {
        $this->lastLoginDate = $lastLoginDate;
    }

    /**
     * @return ArrayCollection
     */
    public function getDogs()
    {
        return $this->dogs;
    }

    /**
     * @param Dog $dog
     */
    public function addDog(Dog $dog)
    {
        $this->dogs->add($dog);
    }

    /**
     * @return ArrayCollection
     */
    public function getRatings()
    {
        return $this->ratings;
    }

    /**
     * @param Rating $rating
     */
    public function addRating(Rating $rating)
    {
        $this->ratings->add($rating);
    }

    /**
     * @return Role
     */
    public function getRole()
    {
        if (is_null($this->role)) {
            $this->role = RoleManager::create($this->roleName);
        }

        return $this->role;
    }

    /**
     * @param Role $role
     */
    public function setRole(Role $role)
    {
        $this->role = $role;
    }
}
