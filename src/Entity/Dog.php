<?php

namespace Wakadog\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Wakadog\Repository\Database\DogRepository")
 * @ORM\Table(name="dogs", indexes={
 *     @ORM\Index(name="coordinates", columns={"latitude", "longitude"})
 * })
 */
class Dog
{
    const ACTIVITY_NEED_SMALL = 0;
    const ACTIVITY_NEED_MODERATE = 1;
    const ACTIVITY_NEED_BIG = 2;

    const RELATIONS_FRIENDLY = 0;
    const RELATIONS_NEUTRAL = 1;
    const RELATIONS_AGGRESSIVE = 2;

    const ARCHIVED_NOT_YET = 0;
    const ARCHIVED_BY_USER = 1;
    const ARCHIVED_BY_MOD = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $latitude;

    /**
     * @ORM\Column(type="float")
     */
    private $longitude;

    /**
     * @ORM\Column(type="integer")
     */
    private $phone;

    /**
     * @ORM\Column
     */
    private $name;

    /**
     * @ORM\Column
     */
    private $breed;

    /**
     * @ORM\Column(type="boolean")
     */
    private $vaccinated;

    /**
     * @ORM\Column(type="integer")
     */
    private $activity_need;

    /**
     * @ORM\Column(type="integer")
     */
    private $humans_relations;

    /**
     * @ORM\Column(type="integer")
     */
    private $animals_relations;

    /**
     * @ORM\Column(type="boolean")
     */
    private $without_dog_lead;

    /**
     * @ORM\Column
     */
    private $image_url;

    /**
     * @ORM\Column(type="text")
     */
    private $about;

    /**
     * @ORM\Column(type="datetime")
     */
    private $published_date;

    /**
     * @ORM\Column(type="integer")
     */
    private $archived = self::ARCHIVED_NOT_YET;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="dogs")
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity="Rating", mappedBy="dog", cascade={"persist", "remove"})
     */
    private $ratings;

    public function __construct()
    {
        $this->ratings = new ArrayCollection();

        $this->published_date = new DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getBreed()
    {
        return $this->breed;
    }

    /**
     * @param mixed $breed
     */
    public function setBreed($breed)
    {
        $this->breed = $breed;
    }

    /**
     * @return mixed
     */
    public function getVaccinated()
    {
        return $this->vaccinated;
    }

    /**
     * @param mixed $vaccinated
     */
    public function setVaccinated($vaccinated)
    {
        $this->vaccinated = $vaccinated;
    }

    /**
     * @return mixed
     */
    public function getActivityNeed()
    {
        return $this->activity_need;
    }

    /**
     * @param mixed $activity_need
     */
    public function setActivityNeed($activity_need)
    {
        $this->activity_need = $activity_need;
    }

    /**
     * @return mixed
     */
    public function getHumansRelations()
    {
        return $this->humans_relations;
    }

    /**
     * @param mixed $humans_relations
     */
    public function setHumansRelations($humans_relations)
    {
        $this->humans_relations = $humans_relations;
    }

    /**
     * @return mixed
     */
    public function getAnimalsRelations()
    {
        return $this->animals_relations;
    }

    /**
     * @param mixed $animals_relations
     */
    public function setAnimalsRelations($animals_relations)
    {
        $this->animals_relations = $animals_relations;
    }

    /**
     * @return mixed
     */
    public function getWithoutDogLead()
    {
        return $this->without_dog_lead;
    }

    /**
     * @param mixed $without_dog_lead
     */
    public function setWithoutDogLead($without_dog_lead)
    {
        $this->without_dog_lead = $without_dog_lead;
    }

    /**
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * @param mixed $image_url
     */
    public function setImageUrl($image_url)
    {
        $this->image_url = $image_url;
    }

    /**
     * @return mixed
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * @param mixed $about
     */
    public function setAbout($about)
    {
        $this->about = $about;
    }

    /**
     * @return mixed
     */
    public function getPublishedDate()
    {
        return $this->published_date;
    }

    /**
     * @param mixed $published_date
     */
    public function setPublishedDate(DateTime $published_date)
    {
        $this->published_date = $published_date;
    }

    /**
     * @return int
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * @param int $archived
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return ArrayCollection
     */
    public function getRatings()
    {
        return $this->ratings;
    }

    /**
     * @param Rating $rating
     */
    public function setRatings(Rating $rating)
    {
        $this->ratings->add($rating);
    }

    public function getAverageRating()
    {
        $sum = 0;

        foreach ($this->ratings as $rating) {
            $sum += $rating->getRating();
        }

        return $sum / (count($this->ratings) ?: 1);
    }
}
