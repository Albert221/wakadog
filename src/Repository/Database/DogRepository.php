<?php

namespace Wakadog\Repository\Database;

use Doctrine\ORM\EntityRepository;
use Wakadog\Entity\Dog;
use Wakadog\Repository\DogRepository as DogRepositoryInterface;

class DogRepository extends EntityRepository implements DogRepositoryInterface
{
    public function find($id)
    {
        /** @var Dog $dog */
        $dog = parent::find($id);

        if ($dog->getOwner()->isBanned()) {
            return null;
        }

        return $dog;
    }

    public function findAll()
    {
        return $this->createQueryBuilder('d')
                    ->join('d.owner', 'o')
                    ->where('o.banned = 0')
                    ->getQuery()
                    ->getResult();
    }

    /**
     * {@inheritdoc}
     * @see http://stackoverflow.com/a/7783777/3158312
     * @fixme This doesn't work I don't know why :(
     */
    public function withinRadius($radius, $latitude, $longitude)
    {
        return $this->createQueryBuilder('d')
                    ->where('acos(sin(d.latitude * 0.0175) * sin(:latitude * 0.0175) + cos(d.latitude * 0.0175) * (:latitude * 0.0175) * cos((:longitude * 0.0175) - (d.longitude * 0.0175))) * 3959 <= :radius')
                    ->setParameter(':latitude', $latitude)
                    ->setParameter(':longitude', $longitude)
                    ->setParameter(':radius', $radius)
                    ->getQuery()
                    ->getResult();
    }

    public function save(Dog $dog)
    {
        $this->getEntityManager()->persist($dog);
        $this->getEntityManager()->flush($dog);
    }

    public function remove(Dog $dog)
    {
        $this->getEntityManager()->remove($dog);
        $this->getEntityManager()->flush($dog);
    }
}
