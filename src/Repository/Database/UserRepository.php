<?php

namespace Wakadog\Repository\Database;

use Doctrine\ORM\EntityRepository;
use Wakadog\Entity\User;
use Wakadog\Repository\UserRepository as UserRepositoryInterface;

class UserRepository extends EntityRepository implements UserRepositoryInterface
{
    /**
     * @param string $email
     * @return User
     */
    public function byEmail($email)
    {
        return $this->createQueryBuilder('u')
                    ->where('u.email = :email')
                    ->setParameter(':email', $email)
                    ->getQuery()
                    ->getSingleResult();
    }

    public function save(User $user)
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush($user);
    }

    public function remove(User $user)
    {
        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush($user);
    }
}
