<?php

namespace Wakadog\Repository\Database;

use Doctrine\ORM\EntityRepository;
use Wakadog\Entity\Dog;
use Wakadog\Entity\Rating;
use Wakadog\Entity\User;
use Wakadog\Repository\RatingRepository as RatingRepositoryInterface;

class RatingRepository extends EntityRepository implements RatingRepositoryInterface
{
    public function lastFive(Dog $dog)
    {
        return $this->createQueryBuilder('r')
                    ->where('r.dog = :dog')
                    ->setParameter('dog', $dog)
                    ->setMaxResults(5)
                    ->getQuery()
                    ->getResult();
    }

    public function byDogAndAuthor(Dog $dog, User $author)
    {
        return $this->createQueryBuilder('r')
            ->where('r.dog = :dog')
            ->andWhere('r.author = :author')
            ->setParameter(':dog', $dog)
            ->setParameter(':author', $author)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function alreadyRated(Dog $dog, User $author)
    {
        return (bool)$this->createQueryBuilder('r')
                    ->select('COUNT(r.id)')
                    ->where('r.dog = :dog')
                    ->andWhere('r.author = :author')
                    ->setParameter(':dog', $dog)
                    ->setParameter(':author', $author)
                    ->getQuery()
                    ->getSingleScalarResult();
    }

    public function save(Rating $rating)
    {
        $this->getEntityManager()->persist($rating);
        $this->getEntityManager()->flush($rating);
    }

    public function remove(Rating $rating)
    {
        $this->getEntityManager()->remove($rating);
        $this->getEntityManager()->flush($rating);
    }
}
