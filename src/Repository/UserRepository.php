<?php

namespace Wakadog\Repository;

use Wakadog\Entity\User;

interface UserRepository
{
    /**
     * @param int $id
     * @return User
     */
    public function find($id);

    /**
     * @param string $email
     * @return User
     */
    public function byEmail($email);

    /**
     * @param User $user
     */
    public function save(User $user);

    /**
     * @param User $user
     */
    public function remove(User $user);
}
