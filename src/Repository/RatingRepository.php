<?php

namespace Wakadog\Repository;

use Wakadog\Entity\Dog;
use Wakadog\Entity\Rating;
use Wakadog\Entity\User;

interface RatingRepository
{
    /**
     * @param int $id
     * @return Rating
     */
    public function find($id);

    /**
     * @param Dog $dog
     * @return Rating[]
     */
    public function lastFive(Dog $dog);

    /**
     * @param Dog $dog
     * @param User $author
     * @return Rating
     */
    public function byDogAndAuthor(Dog $dog, User $author);

    /**
     * @param Dog $dog
     * @param User $author
     * @return bool
     */
    public function alreadyRated(Dog $dog, User $author);

    /**
     * @param Rating $rating
     */
    public function save(Rating $rating);

    /**
     * @param Rating $rating
     */
    public function remove(Rating $rating);
}
