<?php

namespace Wakadog\Repository;

use Wakadog\Entity\Dog;

interface DogRepository
{
    /**
     * @param int $id
     * @return Dog
     */
    public function find($id);

    /**
     * @return Dog[]
     */
    public function findAll();

    /**
     * @param float $radius
     * @param float $latitude
     * @param float $longitude
     * @return Dog[]
     */
    public function withinRadius($radius, $latitude, $longitude);

    /**
     * @param Dog $dog
     */
    public function save(Dog $dog);

    /**
     * @param Dog $dog
     */
    public function remove(Dog $dog);
}
