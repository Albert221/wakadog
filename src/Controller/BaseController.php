<?php

namespace Wakadog\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Interfaces\RouterInterface;
use Slim\Views\Twig;
use Wakadog\Flash\Messages as Flash;
use Wakadog\Security\Session\UserSessionManager;

abstract class BaseController
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var Flash
     */
    protected $flash;

    /**
     * @var UserSessionManager
     */
    private $userSessionManager;

    /**
     * @var RouterInterface
     */
    private $router;

    public function setBaseParams(Twig $twig, Flash $flash, UserSessionManager $userSessionManager, RouterInterface $router)
    {
        $this->twig = $twig;
        $this->flash = $flash;
        $this->userSessionManager = $userSessionManager;
        $this->router = $router;
    }

    protected function render(Response $response, $name, $data = [])
    {
        return $this->twig->render($response, $name, $data);
    }

    protected function getCurrentUser()
    {
        return $this->userSessionManager->getUser();
    }

    protected function pathFor($route, $data = [], $queryParams = [])
    {
        return $this->router->pathFor($route, $data, $queryParams);
    }
}
