<?php

namespace Wakadog\Controller;

use Eventviva\ImageResize;
use Psr\Http\Message\UploadedFileInterface;
use Slim\Http\Request;
use Psr\Http\Message\ResponseInterface as Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use Wakadog\Entity\Dog;
use Wakadog\ForbiddenException;
use Wakadog\Repository\DogRepository;
use Wakadog\Repository\RatingRepository;

class DogController extends BaseController
{
    /**
     * @var DogRepository
     */
    private $dogRepository;

    /**
     * @var RatingRepository
     */
    private $ratingRepository;

    public function __construct(DogRepository $dogRepository, RatingRepository $ratingRepository)
    {
        $this->dogRepository = $dogRepository;
        $this->ratingRepository = $ratingRepository;
    }

    public function map(Request $request, Response $response)
    {
        $dogs = $this->dogRepository->findAll();

        return $this->render($response, 'dog/map.twig', compact('dogs'));
    }

    public function dashboard(Request $request, Response $response)
    {
        return $this->render($response, 'dog/dashboard.twig');
    }

    public function add(Request $request, Response $response)
    {
        return $this->render($response, 'dog/add.twig', [
            'errors' => $this->flash->getMessage('errors'),
            'old' => $this->flash->getMessage('old')
        ]);
    }

    public function store(Request $request, Response $response)
    {
        // TODO: Separate this validation to somewhere else
        $validator = Validation::createValidator();
        $errors = [];

        $phone = str_ireplace(' ', '', $request->getParsedBodyParam('phone'));

        $errors['phone'] = $validator->validate($phone, [
            new Assert\NotBlank(['message' => 'Pole numer telefonu nie może być puste']),
            new Assert\Regex(['message' => 'Numer telefonu jest nieprawidłowy', 'value' => '/^(?:[0+]48)?(\d{9})$/'])
        ]);

        $errors['coordinates'] = $validator->validate($coordinates = $request->getParsedBodyParam('coordinates'), [
            new Assert\NotBlank(['message' => 'Pole koordynaty nie może być puste']),
            // Pattern: http://regexr.com/3efvc matching coordinates in "lat, lng" format
            new Assert\Regex([
                    'message' => 'Pole koordynaty jest nieprawidłowe. Naciśnij miejsce na mapie',
                    'value' => '/^(-?(([0-9]|[0-9][0-9]|1[0-7][0-0])(\.\d*)?|180)),\s(-?(([0-9]|[0-8][0-9])(\.\d*)|90))$/'
                ])
        ]);

        $errors['name'] = $validator->validate($name = $request->getParsedBodyParam('name'), [
            new Assert\NotBlank(['message' => 'Pole imię psiaka nie może być puste'])
        ]);

        $errors['breed'] = $validator->validate($breed = $request->getParsedBodyParam('breed'), [
            new Assert\NotBlank(['message' => 'Pole rasa psiaka nie może być puste'])
        ]);

        $vaccinated = (bool)$request->getParsedBodyParam('vaccinated');

        $errors['activityAmount'] = $validator->validate($activityAmount = (int)$request->getParsedBodyParam('activity_amount'), [
            new Assert\NotBlank(['message' => 'Pole potrzebuje ruchu nie może być puste']),
            new Assert\Range(['min' => 0, 'max' => 2])
        ]);

        $errors['humanRelations'] = $validator->validate($humanRelations = (int)$request->getParsedBodyParam('human_relations'), [
            new Assert\NotBlank(['message' => 'Pole stosunek do ludzi nie może być puste']),
            new Assert\Range(['min' => 0, 'max' => 2])
        ]);

        $errors['animalsRelations'] = $validator->validate($animalsRelations = (int)$request->getParsedBodyParam('animals_relations'), [
            new Assert\NotBlank(['message' => 'Pole stosunek do zwierząt nie może być puste']),
            new Assert\Range(['min' => 0, 'max' => 2])
        ]);

        $withoutDogLead = (bool)$request->getParsedBodyParam('without_dog_lead');

        $image = $request->getUploadedFiles()['image'];
        $imagePath = isset($request->getUploadedFiles()['image']) ? $request->getUploadedFiles()['image']->file : '';
        $errors['image'] = $validator->validate($imagePath, [
            new Assert\NotBlank(['message' => 'Pole na zdjęcie nie może być puste'])
        ]);

        $errors['about'] = $validator->validate($about = $request->getParsedBodyParam('about'), [
            new Assert\NotBlank(['message' => 'Pole o psiaku nie może być puste']),
            new Assert\Length(['maxMessage' => 'Pole o psiaku może zawierać maksymalnie 1000 znaków', 'max' => 1000])
        ]);

        $errorsCount = 0;
        foreach ($errors as $errorField) {
            $errorsCount += count($errorField);
        }

        if ($errorsCount > 0) {
            $this->flash->setMessage('errors', $errors);

            $image = $image && $image->getClientFilename() ?: '';
            $this->flash->setMessage('old', compact(
                'phone',
                'coordinates',
                'name',
                'breed',
                'vaccinated',
                'activityAmount',
                'humanRelations',
                'animalsRelations',
                'withoutDogLead',
                'image',
                'about'
            ));

            return $response->withStatus(302)->withHeader('Location', $this->pathFor('dog.add'));
        }

        $fileTypeExtensions = [
            'image/jpeg' => '.jpg',
            'image/png' => '.png'
        ];

        $fileName = uniqid().$fileTypeExtensions[$image->getClientMediaType()];
        $imageUrl = $request->getUri()->getBasePath().'/storage/'.$fileName;
        $imagePath = __DIR__.'/../../web/storage/'.$fileName;

        $image = new ImageResize($image->file);
        $image->crop(640, 400, true);
        $image->save($imagePath);

        list($latitude, $longitude) = explode(', ', $coordinates);

        $dog = new Dog();
        $dog->setPhone($phone);
        $dog->setLatitude($latitude);
        $dog->setLongitude($longitude);
        $dog->setName($name);
        $dog->setBreed($breed);
        $dog->setVaccinated($vaccinated);
        $dog->setActivityNeed($activityAmount);
        $dog->setHumansRelations($humanRelations);
        $dog->setAnimalsRelations($animalsRelations);
        $dog->setWithoutDogLead($withoutDogLead);
        $dog->setImageUrl($imageUrl);
        $dog->setAbout($about);
        $dog->setOwner($this->getCurrentUser());

        $this->dogRepository->save($dog);

        $this->flash->addMessage('toast', 'Psiak dodany pomyślnie');

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('dog.dog', ['id' => $dog->getId()]));
    }

    public function dog(Request $request, Response $response, $id)
    {
        $dog = $this->dogRepository->find($id);

        if (! $dog) {
            return $response->withStatus(404);
        }

        $ratings = $this->ratingRepository->lastFive($dog);

        return $this->render($response, 'dog/dog.twig', compact('dog', 'ratings'));
    }

    public function edit(Request $request, Response $response, $id)
    {
        $dog = $this->dogRepository->find($id);

        if (! $dog) {
            return $response->withStatus(404);
        }

        return $this->render($response, 'dog/edit.twig', [
            'dog' => $dog,
            'errors' => $this->flash->getMessage('errors'),
            'old' => $this->flash->getMessage('old')
        ]);
    }

    public function update(Request $request, Response $response, $id)
    {
        $dog = $this->dogRepository->find($id);

        if (! $dog) {
            return $response->withStatus(404);
        }

        // TODO: Separate this validation to somewhere else
        $validator = Validation::createValidator();
        $errors = [];

        $phone = str_ireplace(' ', '', $request->getParsedBodyParam('phone'));

        $errors['phone'] = $validator->validate($phone, [
            new Assert\NotBlank(['message' => 'Pole numer telefonu nie może być puste']),
            new Assert\Regex(['message' => 'Numer telefonu jest nieprawidłowy', 'value' => '/^(?:[0+]48)?(\d{9})$/'])
        ]);

        $errors['coordinates'] = $validator->validate($coordinates = $request->getParsedBodyParam('coordinates'), [
            new Assert\NotBlank(['message' => 'Pole koordynaty nie może być puste']),
            // Pattern: http://regexr.com/3efvc matching coordinates in "lat, lng" format
            new Assert\Regex([
                'message' => 'Pole koordynaty jest nieprawidłowe. Naciśnij miejsce na mapie',
                'value' => '/^(-?(([0-9]|[0-9][0-9]|1[0-7][0-0])(\.\d*)?|180)),\s(-?(([0-9]|[0-8][0-9])(\.\d*)|90))$/'
            ])
        ]);

        $errors['name'] = $validator->validate($name = $request->getParsedBodyParam('name'), [
            new Assert\NotBlank(['message' => 'Pole imię psiaka nie może być puste'])
        ]);

        $errors['breed'] = $validator->validate($breed = $request->getParsedBodyParam('breed'), [
            new Assert\NotBlank(['message' => 'Pole rasa psiaka nie może być puste'])
        ]);

        $vaccinated = (bool)$request->getParsedBodyParam('vaccinated');

        $errors['activityAmount'] = $validator->validate($activityAmount = (int)$request->getParsedBodyParam('activity_amount'), [
            new Assert\NotBlank(['message' => 'Pole potrzebuje ruchu nie może być puste']),
            new Assert\Range(['min' => 0, 'max' => 2])
        ]);

        $errors['humanRelations'] = $validator->validate($humanRelations = (int)$request->getParsedBodyParam('human_relations'), [
            new Assert\NotBlank(['message' => 'Pole stosunek do ludzi nie może być puste']),
            new Assert\Range(['min' => 0, 'max' => 2])
        ]);

        $errors['animalsRelations'] = $validator->validate($animalsRelations = (int)$request->getParsedBodyParam('animals_relations'), [
            new Assert\NotBlank(['message' => 'Pole stosunek do zwierząt nie może być puste']),
            new Assert\Range(['min' => 0, 'max' => 2])
        ]);

        $withoutDogLead = (bool)$request->getParsedBodyParam('without_dog_lead');

        /** @var UploadedFileInterface $image */
        $image = $request->getUploadedFiles()['image'];

        $errors['about'] = $validator->validate($about = $request->getParsedBodyParam('about'), [
            new Assert\NotBlank(['message' => 'Pole o psiaku nie może być puste']),
            new Assert\Length(['maxMessage' => 'Pole o psiaku może zawierać maksymalnie 1000 znaków', 'max' => 1000])
        ]);

        $errorsCount = 0;
        foreach ($errors as $errorField) {
            $errorsCount += count($errorField);
        }

        if ($errorsCount > 0) {
            $this->flash->setMessage('errors', $errors);

            $image = $image && $image->getClientFilename() ?: '';
            $this->flash->setMessage('old', compact(
                'phone',
                'coordinates',
                'name',
                'breed',
                'vaccinated',
                'activityAmount',
                'humanRelations',
                'animalsRelations',
                'withoutDogLead',
                'image',
                'about'
            ));

            return $response->withStatus(302)->withHeader('Location', $this->pathFor('dog.edit', ['id' => $id]));
        }

        if ($image->getError() == UPLOAD_ERR_OK) {
            $fileTypeExtensions = [
                'image/jpeg' => '.jpg',
                'image/png' => '.png'
            ];

            $fileName = uniqid() . $fileTypeExtensions[$image->getClientMediaType()];
            $imageUrl = $request->getUri()->getBasePath() . '/storage/' . $fileName;
            $imagePath = __DIR__ . '/../../web/storage/' . $fileName;

            $image = new ImageResize($image->file);
            $image->crop(640, 400, true);
            $image->save($imagePath);

            $dog->setImageUrl($imageUrl);
        }

        list($latitude, $longitude) = explode(', ', $coordinates);

        $dog->setPhone($phone);
        $dog->setLatitude($latitude);
        $dog->setLongitude($longitude);
        $dog->setName($name);
        $dog->setBreed($breed);
        $dog->setVaccinated($vaccinated);
        $dog->setActivityNeed($activityAmount);
        $dog->setHumansRelations($humanRelations);
        $dog->setAnimalsRelations($animalsRelations);
        $dog->setWithoutDogLead($withoutDogLead);
        $dog->setAbout($about);

        $this->dogRepository->save($dog);

        $this->flash->addMessage('toast', 'Zaktualizowano psiaka pomyślnie');

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('dog.dog', ['id' => $id]));
    }

    public function remove(Request $request, Response $response, $id)
    {
        $dog = $this->dogRepository->find($id);

        if (! $dog) {
            return $response->withStatus(404);
        }

        if ($dog->getOwner() != $this->getCurrentUser() && ! $this->getCurrentUser()->getRole()->can('moderate.removeDogs')) {
            throw new ForbiddenException();
        }

        $this->dogRepository->remove($dog);

        $this->flash->addMessage('toast', 'Psiak usunięty pomyślnie');

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('dog.dashboard'));
    }
}
