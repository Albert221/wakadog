<?php

namespace Wakadog\Controller;

use ReflectionClass;
use Slim\Interfaces\RouterInterface;
use Slim\Views\Twig;
use Wakadog\Flash\Messages as Flash;
use Wakadog\Security\Session\UserSessionManager;

class ControllerFactory
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var Flash
     */
    private $flash;

    /**
     * @var UserSessionManager
     */
    private $userSessionManager;

    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(Twig $twig, Flash $flash, UserSessionManager $userSessionManager, RouterInterface $router)
    {
        $this->twig = $twig;
        $this->flash = $flash;
        $this->userSessionManager = $userSessionManager;
        $this->router = $router;
    }

    public function create($className, $parameters = [])
    {
        $reflection = new ReflectionClass($className);

        if (! $reflection->isSubclassOf(BaseController::class)) {
            throw new \Exception(sprintf('Controller to create must be subclass of %s, %s is not.', BaseController::class, $className));
        }

        /** @var BaseController $controller */
        if ($reflection->hasMethod('__construct')) {
            $controller = $reflection->newInstanceArgs($parameters);
        } else {
            $controller = $reflection->newInstanceWithoutConstructor();
        }

        $controller->setBaseParams($this->twig, $this->flash, $this->userSessionManager, $this->router);

        return $controller;
    }
}