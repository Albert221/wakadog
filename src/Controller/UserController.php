<?php

namespace Wakadog\Controller;

use Slim\Http\Request as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Symfony\Component\Validator\Validation;
use Wakadog\Entity\User;
use Wakadog\ForbiddenException;
use Wakadog\Repository\UserRepository;
use Wakadog\Security\Authenticator;
use Symfony\Component\Validator\Constraints as Assert;
use Wakadog\Validator\Constraints\Repeat;

class UserController extends BaseController
{
    /**
     * @var Authenticator
     */
    private $authenticator;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(Authenticator $authenticator, UserRepository $userRepository)
    {
        $this->authenticator = $authenticator;
        $this->userRepository = $userRepository;
    }

    public function signIn(Request $request, Response $response)
    {
        return $this->render($response, 'user/sign-in.twig', [
            'error' => $this->flash->getMessage('error'),
            'old' => [
                'email' => $this->flash->getMessage('old.email')[0]
            ]
        ]);
    }

    public function authenticate(Request $request, Response $response)
    {
        if ($this->authenticator->authenticate(
            $email = $request->getParsedBodyParam('email'),
            $request->getParsedBodyParam('password')
        )) {
            $this->flash->addMessage('toast', 'Zalogowano pomyślnie.');

            return $response->withStatus(302)->withHeader('Location', $this->pathFor('dog.map'));
        }

        $this->flash->addMessage('error', 'Podano błędne dane.');
        $this->flash->addMessage('old.email', $email);

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('user.signIn'));
    }

    public function signUp(Request $request, Response $response)
    {
        return $this->render($response, 'user/sign-up.twig', [
            'errors' => $this->flash->getMessage('errors'),
            'old' => [
                'email' => $this->flash->getMessage('old.email')[0],
                'visibleName' => $this->flash->getMessage('old.visibleName')[0]
            ]
        ]);
    }

    public function register(Request $request, Response $response)
    {
        // TODO: Separate this validation to somewhere else
        $validator = Validation::createValidator();
        $errors = [];

        $errors['email'] = $validator->validate($email = $request->getParsedBodyParam('email'), [
            new Assert\NotBlank(['message' => 'Pole email nie może być puste']),
            new Assert\Email(['message' => 'W polu email musi być prawidłowy adres email'])
        ]);

        $errors['password'] = $validator->validate($password = $request->getParsedBodyParam('password'), [
            new Assert\NotBlank(['message' => 'Pole hasło nie może być puste']),
            new Assert\Length(['minMessage' => 'Hasło musi mieć co najmniej 5 znaków', 'min' => 5])
        ]);

        $errors['confirmPassword'] = $validator->validate($request->getParsedBodyParam('confirm_password'), [
            new Repeat($password)
        ]);

        $errors['visibleName'] = $validator->validate($visibleName = $request->getParsedBodyParam('visible_name'), [
            new Assert\NotBlank(['message' => 'Pole nazwa wyświetlana nie może być puste']),
            new Assert\Length(['minMessage' => 'Nazwa wyświetlana musi mieć co najmniej 3 znaki', 'min' => 3])
        ]);

        $errorsCount = 0;
        foreach ($errors as $errorField) {
            $errorsCount += count($errorField);
        }

        if ($errorsCount > 0) {
            $this->flash->setMessage('errors', $errors);

            $this->flash->addMessage('old.email', $email);
            $this->flash->addMessage('old.visibleName', $visibleName);

            return $response->withStatus(302)->withHeader('Location', $this->pathFor('user.signUp'));
        }

        $user = new User();
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setVisibleName($visibleName);

        $this->userRepository->save($user);

        $this->flash->addMessage('toast', 'Registered successfully');

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('user.signIn'));
    }

    public function signOut(Request $request, Response $response)
    {
        $this->authenticator->logout();

        $this->flash->addMessage('toast', 'Wylogowano pomyślnie.');

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('user.signIn'));
    }

    public function settings(Request $request, Response $response)
    {
        return $this->render($response, 'user/settings.twig', [
            'errors' => $this->flash->getMessage('errors')
        ]);
    }

    public function updateSettings(Request $request, Response $response)
    {
        /** @var User $user */
        $user = $this->getCurrentUser();

        $validator = Validation::createValidator();
        $errors = [];

        $errors['visibleName'] = $validator->validate($visibleName = $request->getParsedBodyParam('visible_name'), [
            new Assert\NotBlank(['message' => 'Pole nazwa wyświetlana nie może być puste']),
            new Assert\Length(['minMessage' => 'Nazwa wyświetlana musi mieć co najmniej 3 znaki', 'min' => 3])
        ]);

        if (count($errors['visibleName']) > 0) {
            $this->flash->setMessage('errors', $errors);

            return $response->withStatus(302)->withHeader('Location', $this->pathFor('user.settings'));
        }

        $user->setVisibleName($visibleName);

        $this->userRepository->save($user);

        $this->flash->addMessage('toast', 'Zaktualizowano dane konta pomyślnie');

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('user.settings'));
    }

    public function changePassword(Request $request, Response $response)
    {
        /** @var User $user */
        $user = $this->getCurrentUser();

        $validator = Validation::createValidator();
        $errors = [];

        if (! password_verify($request->getParsedBodyParam('old_password'), $user->getPassword())) {
            $errors['oldPassword'][]['message'] = 'Stare hasło jest nieprawidłowe';
        }

        $errors['newPassword'] = $validator->validate($newPassword = $request->getParsedBodyParam('password'), [
            new Assert\NotBlank(['message' => 'Pole nowe hasło nie może być puste']),
            new Assert\Length(['minMessage' => 'Hasło musi mieć co najmniej 5 znaków', 'min' => 5])
        ]);

        $errors['confirmNewPassword'] = $validator->validate($request->getParsedBodyParam('confirm_password'), [
            new Repeat(['message' => 'Hasła różnią się', 'value' => $newPassword])
        ]);

        $errorsCount = 0;
        foreach ($errors as $errorField) {
            $errorsCount += count($errorField);
        }

        if ($errorsCount > 0) {
            $this->flash->setMessage('errors', $errors);

            return $response->withStatus(302)->withHeader('Location', $this->pathFor('user.settings'));
        }

        $user->setPassword($newPassword);

        $this->userRepository->save($user);

        $this->flash->addMessage('toast', 'Zmieniono hasło pomyślnie');

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('user.settings'));
    }

    public function deleteAccount(Request $request, Response $response)
    {
        /** @var User $user */
        $user = $this->getCurrentUser();

        $this->userRepository->remove($user);

        $this->authenticator->logout();

        $this->flash->addMessage('toast', 'Konto usunięto pomyślnie');

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('user.signIn'));
    }

    public function ban(Request $request, Response $response, $id)
    {
        $currentUser = $this->getCurrentUser();
        $user = $this->userRepository->find($id);

        if (! $currentUser->getRole()->can('moderate.banUsers') || $user->getRole()->getName() == 'mod') {
            // Except unauthorized requests prevent banning mods
            throw new ForbiddenException();
        }

        $user->setBanned(true);

        $this->userRepository->save($user);

        $this->flash->addMessage('toast', 'Użytkownik zbanowany pomyślnie');

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('dog.dashboard'));
    }
}
