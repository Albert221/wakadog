<?php

namespace Wakadog\Controller;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class LandingController extends BaseController
{
    public function __invoke(Request $request, Response $response)
    {
        return $this->render($response, 'landing.twig');
    }
}
