<?php

namespace Wakadog\Controller;

use Slim\Http\Request;
use Psr\Http\Message\ResponseInterface as Response;
use Wakadog\Entity\Rating;
use Wakadog\Entity\User;
use Wakadog\ForbiddenException;
use Wakadog\Repository\DogRepository;
use Wakadog\Repository\RatingRepository;

class RatingController extends BaseController
{
    /**
     * @var RatingRepository
     */
    private $ratingRepository;

    /**
     * @var DogRepository
     */
    private $dogRepository;

    public function __construct(RatingRepository $ratingRepository, DogRepository $dogRepository)
    {
        $this->ratingRepository = $ratingRepository;
        $this->dogRepository = $dogRepository;
    }

    public function add(Request $request, Response $response)
    {
        $dog = $this->dogRepository->find($request->getParsedBodyParam('dog_id'));
        /** @var User $author */
        $author = $this->getCurrentUser();

        if ($this->ratingRepository->alreadyRated($dog, $author)) {
            $this->flash->addMessage('toast', 'Już oceniłeś tego psiaka');

            return $response->withStatus(302)->withHeader('Location', $this->pathFor('dog.dog', ['id' => $dog->getId()]));
        }

        if ($dog->getOwner() == $author) {
            $this->flash->addMessage('toast', 'Nie możesz oceniać swojego psiaka');

            return $response->withStatus(302)->withHeader('Location', $this->pathFor('dog.dog', ['id' => $dog->getId()]));
        }

        $rating = new Rating();
        $rating->setRating((int)$request->getParsedBodyParam('rating'));
        $rating->setAuthor($author);
        $rating->setDog($dog);

        $this->ratingRepository->save($rating);

        $this->flash->addMessage('toast', 'Pomyślnie oceniono psiaka');

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('dog.dog', ['id' => $dog->getId()]));
    }

    public function remove(Request $request, Response $response, $id)
    {
        $rating = $this->ratingRepository->find($id);

        if (! $rating) {
            return $response->withStatus(404);
        }

        if ($rating->getAuthor() != $this->getCurrentUser() && ! $this->getCurrentUser()->getRole()->can('moderate.removeRatings')) {
            throw new ForbiddenException();
        }

        $this->ratingRepository->remove($rating);

        $this->flash->addMessage('toast', 'Pomyślnie usunięto ocenę psiaka');

        return $response->withStatus(302)->withHeader('Location', $this->pathFor('dog.dog', ['id' => $rating->getDog()->getId()]));
    }
}
